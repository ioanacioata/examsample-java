package model;

import java.util.ArrayList;
import java.util.List;

public class User {
    private String username;
    private String password;
    private String completedTests;
    private List<Integer> completedTestsArray;

    public User(String username, String password) {
        this.username = username;
        this.password = password;
        this.completedTestsArray=new ArrayList<Integer>();
    }

    public User(String username, String password, String completedTests) {
        this(username,password);
        this.completedTests = completedTests;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCompletedTests() {
        return completedTests;
    }

    public void setCompletedTests(String completedTests) {
        this.completedTests = completedTests;
    }

    public List<Integer> getCompletedTestsArray() {
        return completedTestsArray;
    }

    public void setCompletedTestsArray(List<Integer> completedTestsArray) {
        this.completedTestsArray = completedTestsArray;
    }

    @Override
    public String toString() {
        return "User{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", completedTests='" + completedTests + '\'' +
                ", completedTestsArray=" + completedTestsArray +
                '}';
    }
}
