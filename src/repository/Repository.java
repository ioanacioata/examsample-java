package repository;

import model.User;

import java.sql.*;

public class Repository {
    public static final String ERROR_MSG = "error";
    public static final String SUCCESS_MSG = "success";
    private String urlDb;
    private String usernameDb;
    private String passwordDb;
    private String driver;

    public Repository(){
        this.urlDb = "jdbc:mysql://localhost/gamingdb";
        this.usernameDb = "root";
        this.passwordDb = "";
        this.driver = "org.gjt.mm.mysql.Driver";
    }


    public String login(User user) {
        if(user ==null){
            System.out.println("User cannot be null!");
            return ERROR_MSG;
        }

        try {
            Class.forName(driver);
        } catch (ClassNotFoundException e) {
            System.out.println("DRIVER ERROR" + e.getMessage());

        }
        try (Connection connection = DriverManager.getConnection(urlDb, usernameDb, passwordDb);
             PreparedStatement statement = connection.prepareStatement(
                     "SELECT count(*) as count FROM `users` WHERE username=? and password=? ;")) {
            statement.setString(1, user.getUsername());
            statement.setString(2, user.getPassword());
            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    int count = resultSet.getInt("count");
                    System.out.println("count is : "+ count);
                    if(count ==1){
                        return SUCCESS_MSG;
                    }
                    return ERROR_MSG;
                }
            }
        } catch (SQLException e) {
            System.err.println("Failed to save user to DB: " + e.getMessage());
        }
        return ERROR_MSG;
    }
}
