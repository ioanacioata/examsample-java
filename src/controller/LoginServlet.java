package controller;

import model.User;
import repository.Repository;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class LoginServlet extends HttpServlet {
    private Repository repository;

    public LoginServlet() {
        super();
        repository = new Repository();
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        User user = new User(username, password);
        System.out.println(user.toString());

        String status= repository.login(user);

        if(status.equals("success")){
            //login successful
            HttpSession session = request.getSession();
            session.setAttribute("user", user);
            response.sendRedirect("/welcome.jsp");
        }
        else{
            //try again
            RequestDispatcher requestDispatcher=request.getRequestDispatcher("/error.jsp");
            requestDispatcher.forward(request, response);
        }
    }
}
